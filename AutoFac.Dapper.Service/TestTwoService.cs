﻿using AutoFac.Dapper.DataBase;
using AutoFac.Dapper.IService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace AutoFac.Dapper.Service
{
    public class TestTwoService : ITestTwoService
    {
        private readonly DbMasterSession _session;
        private IDbConnection _db;
        public TestTwoService(DbMasterSession session)
        {
            _session = session;
            _db = _session.DbConnection;
        }

        public int GetTableCount()
        {
            var hash = _session.GetHashCode();
            return _db.QueryFirstOrDefault<int>(@"SELECT COUNT(1) FROM tb_Money");
        }
    }
}
