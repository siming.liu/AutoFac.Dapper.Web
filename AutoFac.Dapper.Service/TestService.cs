﻿using AutoFac.Dapper.DataBase;
using AutoFac.Dapper.IService;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Dapper;
namespace AutoFac.Dapper.Service
{
    public class TestService : ITestService
    {
        private readonly DbMasterSession _session;
        private IDbConnection _db;
        public TestService(DbMasterSession session)
        {
            _session = session;
            _db = _session.DbConnection;
        }

        public int GetTableCount()
        {
            var hash = _session.GetHashCode();
            return _db.QueryFirstOrDefault<int>(@"SELECT COUNT(1) FROM tb_User");
        }
    }
}