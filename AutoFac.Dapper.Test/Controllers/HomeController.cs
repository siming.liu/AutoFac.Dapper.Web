﻿using AutoFac.Dapper.DataBase;
using AutoFac.Dapper.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoFac.Dapper.Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITestService testService;
        private readonly ITestTwoService testTwoService;

        public HomeController(ITestService testService, ITestTwoService testTwoService)
        {
            this.testService = testService;
            this.testTwoService = testTwoService;
        }

        public ActionResult Index()
        {
            var count = testService.GetTableCount();
            var count1 = testTwoService.GetTableCount();
            ViewData["Count"] = count;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}