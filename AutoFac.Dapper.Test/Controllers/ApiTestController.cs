﻿using AutoFac.Dapper.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AutoFac.Dapper.Test.Controllers
{
    public class ApiTestController : ApiController
    {
        private readonly ITestService testService;
        private readonly ITestTwoService testTwoService;

        public ApiTestController(ITestService testService, ITestTwoService testTwoService)
        {
            this.testService = testService;
            this.testTwoService = testTwoService;
        }
        // GET: api/ApiTest
        [ActionName("Get")]
        public IEnumerable<int> Get()
        {
            var count = testService.GetTableCount();
            var count1 = testTwoService.GetTableCount();
            return new int[] { count, count1 };
        }
    }
}
