﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFac.Dapper.DataBase
{
    /// <summary>
    /// 公共的数据库连接管理类，用于使用Autofac时可以自动关闭DbConnection
    /// </summary>
    public sealed class DbSlaverSession : IDisposable
    {
        bool _disposed;
        /// <summary>
        /// 用于管理数据库连接
        /// </summary>
        public IDbConnection DbConnection { get; set; }
        public DbSlaverSession()
        {
            DbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SlaversConStr"].ConnectionString);
        }
        /// <summary>
        /// 关闭DbConnection
        /// </summary>
        public void Dispose()
        {
            if (this._disposed)
                return;

            this._disposed = true;

            if (this.DbConnection == null)
                return;

            if (this.DbConnection.State != ConnectionState.Closed)
                this.DbConnection.Close();
        }
    }
}
